import Keycloak, { KeycloakConfig } from 'keycloak-js'
export let keycloak: Keycloak

export function keyCloakCreate (keycloakConfig: KeycloakConfig) {
    keycloak = new Keycloak(keycloakConfig)
}

export async function keyCloakInit (): Promise<void> {
    await keycloak.init({
        onLoad: 'login-required',
        flow: 'implicit',
        checkLoginIframe: true
    })
}

export async function keyCloakLogout () {
    await keycloak.logout({ redirectUri: document.documentURI })
}

export function keyCloakToken () {
    if (keycloak.authenticated) {
        return 'Bearer ' + keycloak.token
    } else {
        return false
    }
}
