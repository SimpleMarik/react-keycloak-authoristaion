import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import {Provider} from 'react-redux';
import {store} from './app/store';
import {authService, $initServices, $auth, $getDisplayedInterface} from './app/helpers';
import {KeycloakConfig} from 'keycloak-js';
import {keyCloakCreate, keyCloakInit, keyCloakToken} from './app/keyCloak';
import {App} from './App';
import {DisplayedInterfaceResponse} from './modules/eventService/models/types/DisplayedInterfaceResponse';

declare global {
    interface Window {
        _env_: {
            OWL_CONF: string;
            OWL_CORE: string;
            OWL_MAIN: string;
        };
    }
}

export const dockerEnv = window._env_;
let displayedInterfaceData: DisplayedInterfaceResponse | null = null;
const displayedWorkModes = new Map<string, boolean>();

export const authorizeUser = async () => {
    try {
        if (document.location.hostname === 'localhost') {
            console.warn('SSO is disabled');
            return;
        }

        const ssoDetails = await authService.GetSsoDetails();

        if (!ssoDetails?.enabled) {
            console.warn('SSO is disabled');
            return;
        }

        const keycloakConfig: KeycloakConfig = {
            url: ssoDetails.ssoUrl,
            realm: ssoDetails.realm,
            clientId: ssoDetails.clientId,
        };

        keyCloakCreate(keycloakConfig);
        await keyCloakInit();
        await $initServices(keyCloakToken());

        displayedInterfaceData = await $getDisplayedInterface();

        if (displayedInterfaceData) {
            const {displayedWorkModes: modes} = displayedInterfaceData;
            modes.forEach(({workModeName, displayed}) => displayedWorkModes.set(workModeName, displayed));
        }

        await $auth();
    } catch (error) {
        console.error('Authorization error:', error);
    }
};

const renderReactApp = () => {
    const rootElement = document.getElementById('root');

    if (!rootElement) {
        console.error('Root element not found');
        return;
    }

    ReactDOM.createRoot(rootElement).render(
        <React.StrictMode>
            <Provider store={store}>
                <App
                    wearableDeviceInterfaceDisplayed={Boolean(displayedInterfaceData?.wearableDeviceInterfaceDisplayed)}
                    displayedWorkModes={displayedWorkModes}
                />
            </Provider>
        </React.StrictMode>
    );
};

const pathname = document.location.pathname;
const inDebuggerMode = pathname === '/dbg' || pathname === '/dbg/';

if (inDebuggerMode && !dockerEnv.OWL_MAIN) {
    renderReactApp();
} else {
    authorizeUser().then(() => renderReactApp());
}
